# privateer

Simple privnote.com clone written in clojure just4fun.
Working version on http://enigmatic-gorge-1190.herokuapp.com

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein ring server

## License

Copyright © 2013 FIXME
