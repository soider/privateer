(ns privateer.main
  (:require [ajax.core :refer [GET POST]] 
            [domina :refer [value by-id destroy-children! append! set-text! remove-class!]]
            [domina.events :refer [listen! prevent-default]]
            [dommy.template :as template]
            [jayq.core :refer [$ delegate toggle fade-in fade-out]]
            [goog.dom :as dom]
            ))

(def storage (.-localStorage js/window))

(def HOSTNAME "http://enigmatic-gorge-1190.herokuapp.com")

(defn hidehelp [] 
   (let [description ($ :.description)
         show-description ($ :#showhelp)]
     (fade-out description 100)
     (-> show-description
       (.removeClass "hidden")
       (fade-in 100)))) 

(defn hidehelp-listener [event]
  (prevent-default event)
  (hidehelp)
  (set! (.-showHelp storage) 0))

(defn showhelp [] 
 (let [description ($ :.description)
        show-description ($ :#showhelp)]  
    (fade-out show-description 100)
    (fade-in  description 100)))

(defn showhelp-listener [event]
  (prevent-default event)
  (showhelp)
  (set! (.-showHelp storage) 1))

(defn generate-link [id]
  (template/node [:input {:type "text" :size 50 :value (str HOSTNAME "/show/" id)}])) 

(defn created-handler [response]
  (let [form ($ :#createform)
        success-holder ($ :#success)
        new-id (response :ok)] 
  (-> success-holder
      (.removeClass "hidden")
      (fade-in 100))
  (fade-out form) 
  (append! (by-id "success") (generate-link new-id)) 
    ))

(defn error-handler [{status :status answer :status-text}]
  (let [error-placeholder ($ :#error-holder)
        form ($ :#createform)]
   (letfn [(generate-error-message [code text] 
             (template/node [:p (str "Error: " code " " text)]))
             ]
    (append! (by-id "error") (generate-error-message status answer))
    (remove-class! (by-id "error") "hidden")
    (fade-in error-placeholder 100)
    )))
 
(defn createnote [event]
  (prevent-default event)
  (let [message (value (by-id "message"))
        title (value (by-id "title"))
        notify (str "fake_host.com");;(value (by-id "notify"))
        die-after (value (by-id "die-after"))
        correct-die-after (re-find  #"^\d+$" (value (by-id "die-after")))
        ]
    (POST "/create" {
                    :params {
                              :message   message
                              :title     title
                              :notify    notify
                              :die-after (if correct-die-after 
                                           (js/parseInt die-after)
                                           1)
                              }
                    :handler created-handler
                    :error-handler error-handler
                    })))

(defn init-help []
  (let [window (dom/getWindow)]
    (let [showHelp (js/parseInt (.-showHelp storage))]
      (if (= 0 showHelp) (hidehelp) (showhelp)))))

;; Use for privateer
(defn ^:export bindevents []
  (init-help)
  (listen! (by-id "hidehelp") :click hidehelp-listener)
  (listen! (by-id "showhelp") :click showhelp-listener)
  (listen! (by-id "submit") :click createnote)
  )
