(ns privateer.util
  (:require [noir.io :as io]
            [markdown.core :as md]))

(defn md->html
  "reads a markdown file from public/md and returns an HTML string"
  [filename]
  (->>
    (io/slurp-resource filename)
    (md/md-to-html-string)))


(defn my-middleware [handler]
  (fn [request]
    (let [response (handler request)]
      (assoc-in response [:headers "X-Middle"] "MyHeadr"))))
