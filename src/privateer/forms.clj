(ns privateer.forms
  (:use hiccup.core)
  (:use hiccup.form))


(defn create-form []
 (html [:form#createform {:action "/create" :method "POST" :name "createform"}
  [:h3 "Your very secret message"]
  [:p [:label {:for "title"} "Title"]]
  [:p [:input#title {:type "text" :name "title"}]]
  [:p [:label {:for "message"} "Message"]]
  [:p [:textarea#message {:cols 50 :rows 4 :name "message"}]]
;;  [:p [:label {:for "notify"} "Notify to when this note gets read"]]
;;  [:p [:input#notify {:name "notify" :type "text"}]]
  [:p [:label {:for "die-after"} "Delete this note after N reads"]]
  [:p [:input#die-after {:value 1 :type "number" :name "die-after"}]]
  [:p [:input#submit {:type "submit" :value "Create"}]]
  ]))
