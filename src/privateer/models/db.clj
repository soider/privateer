(ns privateer.models.db
   (:require 
      [monger.core :as mg]
      [monger.collection :as collection]
      [monger.query :as query]
      [monger.conversion :as conv]))

;; "mongodb://clojurewerkz/monger!:monger!@127.0.0.1/monger-test4"

(def URI "mongodb://privateer:privateer@ds049858.mongolab.com:49858/heroku_app18606246")
(def COLLECTION "documents")

(mg/connect-via-uri! URI)

(defn gen-random-id [] 
  (+ (rand-int 1000) 900))

(defn get-id-from-collection []
  (+ ((conv/from-db-object
     (first (query/with-collection COLLECTION
             (query/find {})
             (query/sort (array-map :value -1))
             (query/limit 1))) 
    true) :id) 1))

(defn to-strange-number [x] x)

(defn from-strange-number [x] x)

(defn String->Number [str]
    (let [n (read-string str)]
             (if (number? n) n nil)))

(defn get-by-id [x] 
  (let [id (from-strange-number (String->Number x))]
    (collection/find-one-as-map COLLECTION {:id id})))

(defn delete-by-id [x]
  (let [id (from-strange-number x)]
    (collection/remove COLLECTION {:id id})))

(defn generate-id []
  (if-not (collection/any? COLLECTION) 
      (gen-random-id)
      (get-id-from-collection)))

(defn decrement-counter [id] 
  (collection/update COLLECTION {:id id} {:$inc {:die-after -1}}))

(defn ^:dynamic new-message [message title notify die-after] 
  (let [new-id (generate-id) 
        notify (clojure.string/trim notify)]
    (binding [new-message {:id new-id 
                           :message message 
                           :title title 
                           :die-after die-after}]
      (if (< 0 (count notify))
        (do (set! new-message (assoc new-message :notify notify))))
        (collection/insert-and-return COLLECTION new-message)
        (to-strange-number new-id)
      )))

