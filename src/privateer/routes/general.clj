(ns privateer.routes.general
  (:use compojure.core)
  (:require [privateer.views.layout :as layout]
            [privateer.util :as util]
            [compojure.route :as route] 
            [privateer.forms :as forms]
            [privateer.models.db :as db]
            [noir.response :as response]
            ))

(defn home-page []
  (layout/render
    "home.html" {:content (util/md->html "/md/home.md")
                 :form  (forms/create-form)
                 }))

(defn about-page []
  (layout/render 
    "about.html" {:content (util/md->html "/md/about.md")}))

(defn create-message [message title notify die-after]
  (let [new-message-id (db/new-message message title notify die-after)]
    {:ok new-message-id}))

(defn real-show-message [message]
  (if (= 1 (message :die-after)) 
        (db/delete-by-id (message :id))
        (db/decrement-counter (message :id)))
  (layout/render 
    "message.html" {:message message}))
 
(defn show-message [id] 
  (let [message (db/get-by-id id)]
      (if message 
          (real-show-message message)
          (route/not-found (layout/render "404.html")))))

(defroutes general-routes
  (GET "/" [] (home-page))
  (GET "/about" [] (about-page))
  (GET "/show/:id" [id] (show-message id))
  (POST "/create" {{title :title message :message notify :notify die-after :die-after} :params} 
        (response/edn (create-message message title notify die-after))
        ))
