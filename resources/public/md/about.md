### About

Have you ever wanted to send confidential information within your work environment, to family or friends, but were afraid to do so over the internet, because some malicious hacker could be spying on you?

Privateer is a free web based service that allows you to send top secret notes over the internet. It's fast, easy, and requires no password or user registration at all.

Just write your note, and you'll get a link. Then you copy and paste that link into an email (or instant message) that you send to the person who you want to read the note. When that person clicks the link for the first time, they will see the note in their browser and the note will automatically self-destruct; which means no one (even that very same person) can read the note again. The link won't work anymore.

You can optionally choose to be notified when your note is read by leaving your email and a reference for the note.

[Give it a try!](/)
