### WTF?

1. Create a note and get a link 
2. Copy the link and send it to whom you want to read the note
3. The note will self-destruct after being read

[Learn more!](/about) 
